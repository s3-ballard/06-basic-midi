/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    audioDeviceManager.setMidiInputEnabled ("Impulse  Impulse", true);
    audioDeviceManager.addMidiInputCallback(String::empty, this);
    addAndMakeVisible(midiLabel);
    
    audioDeviceManager.setDefaultMidiOutput("SimpleSynth virtual input");
    
    midiLabel.setBounds(10, 130, getWidth() -20, getHeight() * 0.5);
    midiLabel.setText("test", dontSendNotification);
    
    addAndMakeVisible(send);
    send.setBounds(10, 170, getWidth() - 20, 40);
    send.setButtonText("Send");
    send.addListener(this);
    
    addAndMakeVisible(midiMessageComp);
    midiMessageComp.setBounds(10, 10, getWidth()-20, 40);
    
    
    
}




MainComponent::~MainComponent()
{
    audioDeviceManager.removeMidiInputCallback(String::empty, this);
    
}

void MainComponent::resized()
{

}

void MainComponent::handleIncomingMidiMessage (MidiInput*, const MidiMessage& message)
{
    DBG("Midi Message Recieved");
    
    String midiText;
    
    
    if (message.isNoteOnOrOff())
    {
        midiText << "NoteOn: Channel " << message.getChannel();
        midiText << ":Number" << message.getNoteNumber();
        midiText << ":Velocity " << message.getVelocity();
    }
    
    if (message.isPitchWheel())
    {
        midiText << ":Pitch Wheel " << message.getPitchWheelValue();
    }
    
    if (message.isAftertouch())
    {
        midiText << ":Aftertouch Value" << message.getAfterTouchValue();
    }
    
    
    
    midiLabel.getTextValue() = midiText;
    
    audioDeviceManager.getDefaultMidiOutput() -> sendMessageNow(message);
    
}



void MainComponent::buttonClicked (Button* button)
{
    //customMess.noteOn(chan, noteNum, vel);
    
    MidiMessage customMess = midiMessageComp.getMidiMessage();
    audioDeviceManager.getDefaultMidiOutput() -> sendMessageNow(customMess);

    
}
