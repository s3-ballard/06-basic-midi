//
//  MidiMessageComponent.cpp
//  JuceBasicWindow
//
//  Created by Scott Ballard on 29/10/2016.
//
//

#include "MidiMessageComponent.hpp"

MidiMessageComponent::MidiMessageComponent()
{
    addAndMakeVisible(messageType);
    messageType.setBounds(10, 10, getWidth()-20, 40);
    messageType.addItem ("Note", 1);
    messageType.addItem ("CC Change", 2);
    
    addAndMakeVisible(Channel);
    Channel.setSliderStyle(Slider::IncDecButtons);
    Channel.setRange (1, 16, 1);
    Channel.setBounds(10, 50, getWidth() -20, 40);
    
    addAndMakeVisible(Number);
    Number.setSliderStyle(Slider::IncDecButtons);
    Number.setRange (0, 127, 1);
    Number.setBounds(10, 90, getWidth()-20, 40);
    
    addAndMakeVisible(Velocity);
    Velocity.setSliderStyle(Slider::IncDecButtons);
    Velocity.setRange(0, 127, 1);
    Velocity.setBounds(10, 130, getWidth()-20, 40);
    
}

MidiMessageComponent::~MidiMessageComponent()
{
    
}

MidiMessage MidiMessageComponent::getMidiMessage()
{
   
    
    int chan = Channel.getValue();
    int noteNum = Number.getValue();
    uint8 vel = Velocity.getValue();
    
    message = MidiMessage::noteOn(chan, noteNum, vel);
    
    return message;
}

/*
void MidiMessageComponent::comboBoxChanged(ComboBox *comboBoxThatHasChanged)
{
    if (comboBoxThatHasChanged == &messageType)
    {
        
        if (messageType.getSelectedId() == 1)
        {
            
        }
        
        else if (messageType.getSelectedId() == 2)
        {
            message = MidiMessage::controllerEvent(<#int channel#>, <#int controllerType#>, <#int value#>)
        }
        
        
    }
    
    
}
 */



