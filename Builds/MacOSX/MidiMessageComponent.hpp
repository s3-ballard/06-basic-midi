//
//  MidiMessageComponent.hpp
//  JuceBasicWindow
//
//  Created by Scott Ballard on 29/10/2016.
//
//

#ifndef MidiMessageComponent_hpp
#define MidiMessageComponent_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

class MidiMessageComponent : public Component,
public ComboBox::Listener

{
public:
    MidiMessageComponent();
    ~MidiMessageComponent();
    
    MidiMessage getMidiMessage();
    
    void comboBoxChanged(ComboBox *comboBoxThatHasChanged) override;
    
    
private:
    Slider Channel;
    Slider Number;
    Slider Velocity;
    ComboBox messageType;
    
    MidiMessage message;
    
    
};

#endif /* MidiMessageComponent_hpp */
